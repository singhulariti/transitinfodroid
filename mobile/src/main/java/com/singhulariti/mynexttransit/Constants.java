package com.singhulariti.mynexttransit;

/**
 * Created by ssingh on 5/6/15.
 */
final public class Constants {
    public static final String NJ_TRANSIT_BUS_SERVICE = "http://54.149.89.156:7020/";
    public static String BY_STOP_CODE_AND_SCHEDULE = "by_stop_code_and_scheduled/";
    public static String NEAREST_AND_SCHEDULED = "nearest_and_scheduled";
    public static String SCHEDULE = "schedule";
    public static String ALL_ROUTES = "all_routes";
    public static String BY_ADDRESS_AND_SCHEDULED = "by_address_and_scheduled";
    public static String ALERTS = "alerts";
    public static enum Timed {
        TIMED, UN_TIMED;
    }
}
