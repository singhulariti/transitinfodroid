package com.singhulariti.mynexttransit;

import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.singhulariti.mynexttransit.tabs.AsyncTaskCompleteListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ScheduleActivity extends ActionBarActivity implements AsyncTaskCompleteListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener
{
    public static final String TIMEPICKER_TAG = "timepicker";
    public static final String DATEPICKER_TAG = "datepicker";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    // LogCat tag
    private static final String TAG = ScheduleActivity.class.getSimpleName();
    private Location mLastLocation;
    double latitude,longitude;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 30000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters
    int time_hour, time_min, year, month, day;
    TimePickerDialog timePicker;
    String route_code = "", address_or_id = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedules);
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();

            createLocationRequest();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        updateRoutesIfNetwork();
        final ImageButton floatingButton = (ImageButton) findViewById(R.id.timeSchedulerButton);
        Calendar now = Calendar.getInstance();
        final TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
        timePicker = tpd;
        final DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("DatePicker", "Dialog was cancelled");
            }
        });
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dpd.show(getFragmentManager(), DATEPICKER_TAG);
            }
        });

        time_hour = now.get(Calendar.HOUR_OF_DAY);
        time_min = now.get(Calendar.MINUTE);
        day = now.get(Calendar.DAY_OF_MONTH);
        month = now.get(Calendar.MONTH) + 1;
        year = now.get(Calendar.YEAR);

        final ImageButton searchButton = (ImageButton) findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateListIfNetwork();
            }
        });

    }

    @Override
    public void onTaskComplete(JSONArray result) throws JSONException {
        TextView textView = (TextView) findViewById(R.id.schedule_label);
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
        textView.setText("Schedule: " + dateFormatSymbols.getMonths()[month - 1] + " " + day
                + " - " + Utils.get24HourFormat(time_hour, time_min) + " to " + Utils.get24HourFormat(time_hour + 1, time_min));
        Utils.updateScheduleList(this, result);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putInt("time_hour", time_hour);
        savedInstanceState.putInt("time_min", time_min);
        savedInstanceState.putInt("year", year);
        savedInstanceState.putInt("month", month);
        savedInstanceState.putInt("day", day);
        savedInstanceState.putString("route_code", route_code);
        savedInstanceState.putString("address_or_id", address_or_id);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        time_hour = savedInstanceState.getInt("time_hour");
        time_min = savedInstanceState.getInt("time_min");
        year = savedInstanceState.getInt("year");
        month = savedInstanceState.getInt("month");
        day = savedInstanceState.getInt("day");
        route_code = savedInstanceState.getString("route_code");
        address_or_id = savedInstanceState.getString("address_or_id");
        updateListIfNetwork();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        time_hour = hourOfDay;
        time_min = minute;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        this.year = year;
        this.month = monthOfYear + 1;
        this.day = dayOfMonth;
        timePicker.show(getFragmentManager(), TIMEPICKER_TAG);
    }

    private void updateRoutesIfNetwork()
    {
        if(isNetworkAvailable()) {
            new Services.GetRouteList(this).execute();
        }
    }

    private void updateListIfNetwork()
    {
        if(isNetworkAvailable()) {
            setLocation();
            Spinner mySpinner=(Spinner) findViewById(R.id.spinner);
            try {
                if (mySpinner.getSelectedItemPosition() != 0) {
                    route_code = mySpinner.getSelectedItem().toString();
                }

                EditText text = (EditText) findViewById(R.id.editText);
                address_or_id = text.getText().toString();
            }catch (Exception e){
            }

            try {
                new Services.GetSchedules(this).execute(latitude + "", longitude+"", time_hour+"",
                        time_min+"", year+"", month+"", day+"", route_code, URLEncoder.encode(address_or_id.trim(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                new Services.GetSchedules(this).execute(latitude + "", longitude+"", time_hour+"",
                        time_min+"", year+"", month+"", day+"",route_code, "");
            }
        }
    }

    public void setBusList(JSONArray buses) throws JSONException {
        List<String> list = new ArrayList<>();
        if (buses != null) {
            list.add("Any Route");
            for (int i = 0; i < buses.length(); i++) {
                list.add(buses.getString(i));
            }
        }

        ArrayAdapter<String> spinnerMenu = new ArrayAdapter<>(getApplicationContext(),  R.layout.spinner_item, list);
        Spinner busesSpinner = (Spinner) findViewById(R.id.spinner);
        busesSpinner.setAdapter(spinnerMenu);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager  = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        boolean isAvailable = false;
        if(networkInfo != null && networkInfo.isConnected()){
            isAvailable = true;
        }else{
            Toast.makeText(this, "Network is unavailable.Please get connected.", Toast.LENGTH_LONG).show();
        }

        return isAvailable;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        checkPlayServices();
        updateListIfNetwork();
        // Resuming the periodic location updates
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    /**
     * Method to display the location on UI
     * */
    private void setLocation() {

        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();

            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();

        } else {
            Toast.makeText(getApplicationContext(), "Couldn't get the location. Make sure location is enabled on the device)", Toast.LENGTH_LONG);
        }
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     * */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getApplicationContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
            }
            return false;
        }
        return true;
    }

    /**
     * Starting the location updates
     * */
    protected void startLocationUpdates() {

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        updateListIfNetwork();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        // Assign the new location
        mLastLocation = location;

        Toast.makeText(getApplicationContext(), "Location changed!",
                Toast.LENGTH_SHORT).show();

        // Displaying the new location on UI
        latitude = mLastLocation.getLatitude();
        longitude = mLastLocation.getLongitude();
    }
}