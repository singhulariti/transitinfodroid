package com.singhulariti.mynexttransit;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.singhulariti.mynexttransit.tabs.AsyncTaskCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static com.singhulariti.mynexttransit.Constants.BY_ADDRESS_AND_SCHEDULED;
import static com.singhulariti.mynexttransit.Constants.BY_STOP_CODE_AND_SCHEDULE;

public class SearchResultsActivity extends ActionBarActivity implements AsyncTaskCompleteListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_results_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY).trim();
            if(query != null) {
                if(containsOnlyNumbers(query)) {
                    new Services.GetRemoteData(this, this).execute(BY_STOP_CODE_AND_SCHEDULE + query + "?",
                            Constants.Timed.TIMED.toString());
                }
                else
                {
                    try {
                        new Services.GetRemoteData(this, this).execute(BY_ADDRESS_AND_SCHEDULED
                                + "?address="
                                + URLEncoder.encode(query, "UTF-8") + "&",
                                Constants.Timed.TIMED.toString());
                    } catch (UnsupportedEncodingException e) {
                        Toast.makeText(getApplicationContext(), "Failed to search, check address or stop id!", Toast.LENGTH_LONG);
                    }
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if(query != null) {
                if(containsOnlyNumbers(query)) {
                    new Services.GetRemoteData(this, this).execute(BY_STOP_CODE_AND_SCHEDULE + query + "?",
                            Constants.Timed.TIMED.toString());
                }
                else
                {
                    try {
                        new Services.GetRemoteData(this, this).execute(BY_ADDRESS_AND_SCHEDULED
                                + "?address="
                                + URLEncoder.encode(query, "UTF-8") + "&",
                                Constants.Timed.TIMED.toString());
                    } catch (UnsupportedEncodingException e) {
                        Toast.makeText(getApplicationContext(), "Failed to search, check address or stop id!", Toast.LENGTH_LONG);
                    }
                }
            }
        }
    }

    @Override
    public void onTaskComplete(JSONArray result) throws JSONException {
        Utils.updateList(this, result);
    }


    @Override
    public boolean onSearchRequested() {
        return false;  // don't go ahead and show the search box
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static boolean containsOnlyNumbers(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i)))
                return false;
        }
        return true;
    }

}