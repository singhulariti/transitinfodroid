package com.singhulariti.mynexttransit;

/**
 * Created by Saurabh on 4/5/14.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.singhulariti.mynexttransit.tabs.AsyncTaskCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import static com.singhulariti.mynexttransit.Constants.*;

public class Services {

    public static class GetRemoteData extends AsyncTask<String,Void,JSONArray> {

        private AsyncTaskCompleteListener callback;
        private ProgressDialog progressDialog;
        private Activity activity;

        public GetRemoteData(AsyncTaskCompleteListener act, Activity activity) {
            this.activity = activity;
            this.callback = act;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(activity, "", "Updating...");
        }

        @Override
        protected JSONArray doInBackground(String... params) {
            JSONArray res;

            if(params[1].equals(Timed.TIMED.toString())) {
               res = getData(params[0], true, 30);
            }
            else
            {
                res = getData(params[0], false, 30);
            }
            return res;
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            super.onPostExecute(result);
            try {
                progressDialog.dismiss();
                callback.onTaskComplete(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class GetSchedules extends AsyncTask<String,Void,JSONArray> {

        private AsyncTaskCompleteListener callback;

        public GetSchedules(AsyncTaskCompleteListener act) {
            this.callback = act;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... params) {
            JSONArray result1;
            try {
                result1 = Utils.getJSONArray(NJ_TRANSIT_BUS_SERVICE + SCHEDULE + "?lat="
                        + params[0] + "&lon=" + params[1] + "&start_hour=" + params[2]
                        + "&start_min=" + params[3]
                        + "&year=" + params[4]
                        + "&month=" + params[5]
                        + "&day=" + params[6]
                        + "&address_or_id=" + params[8]
                        + "&route_code=" + params[7]);
                return result1;
            }catch (Exception e)
            {
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            super.onPostExecute(result);
            try {
                callback.onTaskComplete(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static class GetRouteList extends AsyncTask<String,Void,JSONArray> {

        private ScheduleActivity callback;

        public GetRouteList(ScheduleActivity act) {
            this.callback = act;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... params) {
            JSONArray result1 = Utils.getJSONArray(NJ_TRANSIT_BUS_SERVICE + ALL_ROUTES);
            return result1;
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            super.onPostExecute(result);
            try {
                callback.setBusList(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static JSONArray getData(String url, boolean isTimed, int timeToAdd)
    {
        JSONObject result1;
        String completeUrl = url;
        if (isTimed)
            completeUrl = buildTimedUrl(url, timeToAdd);

        try {
            result1 = Utils.getJSONObject(completeUrl);
            if (result1 == null) return null;
            JSONArray res = new JSONArray();
            res.put(result1);
            return res;
        }catch (Exception e)
        {
            return null;
        }
    }

    private static String buildTimedUrl(String url, int timeToAdd)
    {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE,timeToAdd);
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int min = now.get(Calendar.MINUTE);
        int day = now.get(Calendar.DAY_OF_MONTH);
        int month = now.get(Calendar.MONTH) + 1;
        int year = now.get(Calendar.YEAR);
        String completeUrl = NJ_TRANSIT_BUS_SERVICE
                + url
                + "start_hour=" + hour
                + "&start_min=" + min
                + "&year=" + year
                + "&month=" + month
                + "&day=" + day;
        return completeUrl;
    }
}
