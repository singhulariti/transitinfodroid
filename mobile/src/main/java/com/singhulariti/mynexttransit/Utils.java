package com.singhulariti.mynexttransit;

/**
 * Created by Saurabh on 4/5/14.
 */

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.singhulariti.mynexttransit.adapters.AlertCardItemData;
import com.singhulariti.mynexttransit.adapters.BaseInflaterAdapter;
import com.singhulariti.mynexttransit.adapters.CardItemData;
import com.singhulariti.mynexttransit.adapters.ScheduleCardItemData;
import com.singhulariti.mynexttransit.adapters.inflaters.AlertCardInflater;
import com.singhulariti.mynexttransit.adapters.inflaters.CardInflater;
import com.singhulariti.mynexttransit.adapters.inflaters.ScheduleCardInflater;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    private static String getUrlJsonResponse(String url) {
        String jsonResponse = null;
        HttpURLConnection linkConnection = null;
        try {
            URL linkurl = new URL(url);
            linkConnection = (HttpURLConnection) linkurl.openConnection();
            int responseCode = linkConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = linkConnection.getInputStream();
                Reader reader = new InputStreamReader(inputStream, "UTF-8");
                int contentLength = linkConnection.getContentLength();
                char[] charArray = new char[contentLength];
                int hasRead = 0;
                while (hasRead < contentLength)
                    hasRead += reader.read(charArray, hasRead, contentLength-hasRead);

                inputStream.close();
                jsonResponse = new String(charArray);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (linkConnection != null) {
                linkConnection.disconnect();
            }
        }
        return jsonResponse;
    }

    public static JSONObject getJSONObject(String url) {
        String response = getUrlJsonResponse(url);
        if (response == null)
            return null;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static JSONArray getJSONArray(String url) {
        String response = getUrlJsonResponse(url);
        if (response == null)
            return null;
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public static void updateList(Activity activity, JSONArray result) throws JSONException {
        if (activity != null) {
            BaseInflaterAdapter<CardItemData> adapter = new BaseInflaterAdapter<>(new CardInflater());
            ListView listView = (ListView) activity.findViewById(R.id.list);
            listView.addHeaderView(new View(activity));
            listView.addFooterView(new View(activity));
            if (result != null && result.getJSONObject(0)!= null && result.getJSONObject(0).has("real_time") && result.getJSONObject(0).getJSONObject("real_time").has("current_routes")) {
                JSONArray routes = result.getJSONObject(0).getJSONObject("real_time").getJSONArray("current_routes");
                if (routes != JSONObject.NULL && routes.length() > 0) {
                    for (int i = 0; i < routes.length(); i++) {
                        JSONObject jObj = (JSONObject) routes.get(i);
                        String time = "";
                        try {
                            time = jObj.getString("time_number");
                        } catch (NumberFormatException ne) {
                            time = "";
                        }

                        CardItemData data = new CardItemData(jObj.getString("route_id"), time,
                                jObj.getString("time_unit"), jObj.getString("current_location"));
                        adapter.addItem(data, false);

                    }
                }
            } else {
                CardItemData data = new CardItemData("No Services", "noservice", "", "");
                adapter.addItem(data, false);
            }
            if (result != null && result.getJSONObject(0)!= null && result.getJSONObject(0).has("real_time") && result.getJSONObject(0).getJSONObject("real_time").has("stop_code")) {
                TextView txtView = (TextView)activity.findViewById(R.id.textView);
                txtView.setText(activity.getResources().getText(R.string.buses_closest) + System.getProperty("line.separator") +
                        "Stop Id: " + result.getJSONObject(0).getJSONObject("real_time").getString("stop_code"));
            }
            listView.setAdapter(adapter);
            ListUtils.setDynamicHeight(listView);
            JSONArray scheduleArray = new JSONArray();
            if(result != null && result.getJSONObject(0) != null && result.getJSONObject(0).has("scheduled"))
                scheduleArray = result.getJSONObject(0).getJSONArray("scheduled");
            updateScheduleList(activity, scheduleArray);
        }
    }

    public static void updateScheduleList(Activity activity, JSONArray result) throws JSONException {
        if (activity != null){
            BaseInflaterAdapter<ScheduleCardItemData> adapter = new BaseInflaterAdapter<>(new ScheduleCardInflater());
            ListView listView = (ListView) activity.findViewById(R.id.scheduleList);
            if (result != null && result.length() > 0) {
                JSONArray routes = result;
                if (routes != JSONObject.NULL && routes.length() > 0) {
                    for (int i = 0; i < routes.length(); i++) {
                        JSONObject jObj = (JSONObject) routes.get(i);
                        TextView txtView = (TextView)activity.findViewById(R.id.schedule_label);
                        if(i == 0 && jObj.getString("stop_code") != null && txtView != null)
                        {

                            txtView.setText(activity.getResources().getText(R.string.search_results) + System.getProperty("line.separator") +
                                    "Stop Id: " + jObj.getString("stop_code"));
                        }
                        ScheduleCardItemData data = new ScheduleCardItemData(jObj.getString("route"),
                                jObj.getString("arrival_time"), jObj.getString("headsign"));
                        adapter.addItem(data, false);
                    }
                }
            } else {
                ScheduleCardItemData data = new ScheduleCardItemData("No Services", "", "");
                adapter.addItem(data, false);
            }

            listView.setAdapter(adapter);
            ListUtils.setDynamicHeight(listView);
        }
    }

    public static void updateAlertsList(Activity activity, JSONArray result) throws JSONException {
        if(activity != null) {
            BaseInflaterAdapter<AlertCardItemData> adapter = new BaseInflaterAdapter<>(new AlertCardInflater());
            ListView listView = (ListView) activity.findViewById(R.id.alerts_list);
            listView.addHeaderView(new View(activity));
            listView.addFooterView(new View(activity));
            if (result != null) {
                JSONArray routes = result.getJSONObject(0).getJSONArray("current_alerts");
                if (routes != JSONObject.NULL && routes.length() > 0) {
                    for (int i = 0; i < routes.length(); i++) {
                        JSONObject jObj = (JSONObject) routes.get(i);
                        AlertCardItemData data = new AlertCardItemData(jObj.getString("title"),
                                jObj.getString("updated"), jObj.getString("description"));
                        adapter.addItem(data, false);
                    }
                }
            } else {
                AlertCardItemData data = new AlertCardItemData("No Services!", "", "");
                adapter.addItem(data, false);
            }
            listView.setAdapter(adapter);
        }
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public static String get24HourFormat(int time_hour, int time_min)
    {
        String time = time_hour + ":" + time_min + ":00";
        DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = null;
        try {
            d = f1.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            return time_hour + ":" + time_min;
        }
        DateFormat f2 = new SimpleDateFormat("h:mm a");
        return f2.format(d).toLowerCase();
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
}
