package com.singhulariti.mynexttransit.adapters;

/**
 * Created by Justin on 2/2/14.
 */
public class CardItemData
{
	private String stopId;
	private String time;
    private String timeUnit;
	private String goesTo;

	public CardItemData(String stopId, String time, String timeUnit, String goesTo)
	{
		this.stopId = stopId;
		this.time = time;
        this.timeUnit = timeUnit;
		this.goesTo = goesTo;
	}

	public String getStopId()
	{
		return stopId;
	}

	public String getTime()
	{
		return time;
	}

    public String getTimeUnit()
    {
        return timeUnit;
    }

	public String getGoesTo()
	{
		return goesTo;
	}
}
