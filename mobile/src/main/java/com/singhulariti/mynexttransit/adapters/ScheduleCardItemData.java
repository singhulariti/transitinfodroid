package com.singhulariti.mynexttransit.adapters;

/**
 * Created by Justin on 2/2/14.
 */
public class ScheduleCardItemData
{
	private String title;
	private String time;
    private String description;

	public ScheduleCardItemData(String stopId, String time, String description)
	{
		this.title = stopId;
		this.time = time;
        this.description = description;
	}

	public String getTitle()
	{
		return title;
	}

	public String getTime()
	{
		return time;
	}

    public String getDescription()
    {
        return description;
    }
}
