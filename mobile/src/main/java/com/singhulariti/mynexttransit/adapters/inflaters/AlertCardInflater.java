package com.singhulariti.mynexttransit.adapters.inflaters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.singhulariti.mynexttransit.R;
import com.singhulariti.mynexttransit.adapters.BaseInflaterAdapter;
import com.singhulariti.mynexttransit.adapters.AlertCardItemData;
import com.singhulariti.mynexttransit.adapters.IAdapterViewInflater;

/**
 * Created with IntelliJ IDEA.
 * User: Justin
 * Date: 10/6/13
 * Time: 12:47 AM
 */
public class AlertCardInflater implements IAdapterViewInflater<AlertCardItemData>
{
	@Override
	public View inflate(final BaseInflaterAdapter<AlertCardItemData> adapter, final int pos, View convertView, ViewGroup parent)
	{
		ViewHolder holder;

		if (convertView == null)
		{
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.alert_list_item_card, parent, false);
			holder = new ViewHolder(convertView);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		final AlertCardItemData item = adapter.getTItem(pos);
		holder.updateDisplay(item);

		return convertView;
	}

	private class ViewHolder
	{
		private View routeView;
		private TextView title;
		private TextView description;

		public ViewHolder(View rootView)
		{
			routeView = rootView;
			title = (TextView) routeView.findViewById(R.id.item);
			description = (TextView) routeView.findViewById(R.id.textView1);
			rootView.setTag(this);
		}

		public void updateDisplay(final AlertCardItemData item)
		{
            title.setText(item.getTitle() + " - " + item.getTime());
            description.setText("Description: " + item.getDescription());
		}
	}


}
