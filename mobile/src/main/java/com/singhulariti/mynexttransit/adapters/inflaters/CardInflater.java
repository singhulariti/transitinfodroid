package com.singhulariti.mynexttransit.adapters.inflaters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gc.materialdesign.views.ProgressBarDeterminate;
import com.singhulariti.mynexttransit.R;
import com.singhulariti.mynexttransit.Utils;
import com.singhulariti.mynexttransit.adapters.BaseInflaterAdapter;
import com.singhulariti.mynexttransit.adapters.CardItemData;
import com.singhulariti.mynexttransit.adapters.IAdapterViewInflater;

/**
 * Created with IntelliJ IDEA.
 * User: Justin
 * Date: 10/6/13
 * Time: 12:47 AM
 */
public class CardInflater implements IAdapterViewInflater<CardItemData>
{
	@Override
	public View inflate(final BaseInflaterAdapter<CardItemData> adapter, final int pos, View convertView, ViewGroup parent)
	{
		ViewHolder holder;

		if (convertView == null)
		{
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.list_item_card, parent, false);
			holder = new ViewHolder(convertView);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		final CardItemData item = adapter.getTItem(pos);
		holder.updateDisplay(item);

		return convertView;
	}

	private class ViewHolder
	{
		private View routeView;
		private TextView route_id;
		private TextView towards;
		private ProgressBarDeterminate progressIndicator;

		public ViewHolder(View rootView)
		{
			routeView = rootView;
			route_id = (TextView) routeView.findViewById(R.id.item);
			towards = (TextView) routeView.findViewById(R.id.textView1);
			progressIndicator = (ProgressBarDeterminate) routeView.findViewById(R.id.progressDeterminate);
			rootView.setTag(this);
		}

		public void updateDisplay(final CardItemData item)
		{
			if(Utils.isNumeric(item.getTime()))
            {
                route_id.setText(item.getStopId() + " arrives in " + item.getTime() + " " + item.getTimeUnit());
                towards.setText("Towards: " + item.getGoesTo());
                int time = 0;
                try {
                    time = Integer.parseInt(item.getTime());
                }catch(NumberFormatException ne)
                {

                }
                progressIndicator.setProgress(100-time);
            }
            else if(item.getTime().equals("noservice"))
            {
                route_id.setText("No services at this time!");
                towards.setText("");
                progressIndicator.setProgress(0);
            }
            else
            {
                route_id.setText(item.getStopId() + " " + item.getTimeUnit());
                towards.setText("Towards: " + item.getGoesTo());
                progressIndicator.setProgress(0);
            }
		}
	}


}
