package com.singhulariti.mynexttransit.adapters.inflaters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.singhulariti.mynexttransit.R;
import com.singhulariti.mynexttransit.adapters.BaseInflaterAdapter;
import com.singhulariti.mynexttransit.adapters.IAdapterViewInflater;
import com.singhulariti.mynexttransit.adapters.ScheduleCardItemData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Justin
 * Date: 10/6/13
 * Time: 12:47 AM
 */
public class ScheduleCardInflater implements IAdapterViewInflater<ScheduleCardItemData>
{
	@Override
	public View inflate(final BaseInflaterAdapter<ScheduleCardItemData> adapter, final int pos, View convertView, ViewGroup parent)
	{
		ViewHolder holder;

		if (convertView == null)
		{
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.schedule_list_item_card, parent, false);
			holder = new ViewHolder(convertView);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		final ScheduleCardItemData item = adapter.getTItem(pos);
		holder.updateDisplay(item);

		return convertView;
	}

	private class ViewHolder
	{
		private View routeView;
		private TextView title;
        private TextView description;

		public ViewHolder(View rootView)
		{
			routeView = rootView;
			title = (TextView) routeView.findViewById(R.id.item);
            description = (TextView) routeView.findViewById(R.id.textView1);
			rootView.setTag(this);
		}

		public void updateDisplay(final ScheduleCardItemData item)
		{
            title.setText(item.getTitle() + convertTime(item.getTime()));
            description.setText(item.getDescription());
		}
	}

    private String convertTime(String time)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("hh:mm aa");
        String out = time;
        try {
            Date date = dateFormat.parse(time);

            out = " at " + dateFormat2.format(date);
            Log.e("Time", out);
            } catch (ParseException e) {
        }
        return out;
    }


}
