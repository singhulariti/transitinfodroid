package com.singhulariti.mynexttransit.scheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Map;

/**
 * This BroadcastReceiver automatically (re)starts the alarm when the device is
 * rebooted. This receiver is set to be disabled (android:enabled="false") in the
 * application's manifest file. When the user sets the alarm, the receiver is enabled.
 * When the user cancels the alarm, the receiver is disabled, so that rebooting the
 * device will not trigger this receiver.
 */
// BEGIN_INCLUDE(autostart)
public class BootReceiver extends BroadcastReceiver {
    AlarmReceiver alarm = new AlarmReceiver();
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
            SharedPreferences sharedPref = context.getSharedPreferences("mynexttransit_prefs", Context.MODE_PRIVATE);
            Map<String, ?> allEntries = sharedPref.getAll();
            for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                try {
                    String[] alarmEntry = entry.getValue().toString().split(":");
                    Log.e("MyNextTransit", entry.getValue().toString());
                    alarm.setAlarm(context, Integer.parseInt(alarmEntry[0]),
                            Integer.parseInt(alarmEntry[1]));
                }
                catch(Exception e)
                {
                    Log.e("MyNextTransit", e.getMessage());
                    continue;
                }
            }
        }
    }
}
//END_INCLUDE(autostart)
