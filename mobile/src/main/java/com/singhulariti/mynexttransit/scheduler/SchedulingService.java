package com.singhulariti.mynexttransit.scheduler;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;

import com.singhulariti.mynexttransit.MainActivity;
import com.singhulariti.mynexttransit.R;
import com.singhulariti.mynexttransit.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * This {@code IntentService} does the app's actual work.
 * {@code AlarmReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class SchedulingService extends IntentService implements LocationListener {
    private LocationManager locationManager;
    private String provider;
    double latitude, longitude;
    public static final String NJ_TRANSIT_BUS_SERVICE = "http://54.149.89.156:7020/";

    public SchedulingService() {
        super("SchedulingService");
    }

    public static final String TAG = "Scheduling Demo";
    // An ID used to post the notification.
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;

    @Override
    protected void onHandleIntent(Intent intent) {
        setLocation();
        JSONObject result1 = Utils.getJSONObject(NJ_TRANSIT_BUS_SERVICE + "nearest?lat="
                + latitude + "&lon=" + longitude);

        JSONArray res = new JSONArray();
        res.put(result1);

        sendNotification(buildNotifications(res));
    }

    private List<String> buildNotifications(JSONArray result)
    {
        List<String> builder = new ArrayList<String>();
        if(result != null) {
            JSONArray routes = null;
            try {
                routes = result.getJSONObject(0).getJSONArray("current_routes");

                if (routes != JSONObject.NULL && routes.length() > 0) {
                    for (int i = 0; i < routes.length(); i++) {
                        JSONObject jObj = (JSONObject) routes.get(i);
                        if(Utils.isNumeric(jObj.getString("time_number")))
                        {
                            int time = jObj.getInt("time_number");
                            builder.add(jObj.getString("route_id") + " in " + time + " " + jObj.getString("time_unit"));
                        }
                        else
                        {
                            builder.add(jObj.getString("route_id") + " " + jObj.getString("time_unit"));
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
                builder.add("No Services!");
            }
            if(builder.size() == 0)
                builder.add("No Services!");
        }
        else {
            builder.add("No Services!");
        }
        return builder;
    }

    // Post a notification indicating whether a doodle was found.
    private void sendNotification(List<String> msgs) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name));
        if(msgs.size() > 1) {

            style.addLine(getBoldText(msgs.get(0)));
            style.addLine(getBoldText(msgs.get(1)));

            if(msgs.size() > 2)
            {
                style.setSummaryText( "+" + (msgs.size() - 2) +  " more");
            }
        }
        mBuilder.setStyle(style);
        if(msgs.size() > 1)
        {
            mBuilder.setContentText(getBoldText(msgs.size() + " updates"));
        }
        else {
            mBuilder.setContentText(getBoldText(msgs.get(0)));
        }
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setStyle(style);
        mBuilder.setContentIntent(contentIntent)
                .setSound(uri);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private Spannable getBoldText(String str)
    {
        Spannable sb = new SpannableString(str);
        sb.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(Typeface.SANS_SERIF, 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }

//
// The methods below this line fetch content from the specified URL and return the
// content as a string.
//
    /** Given a URL string, initiate a fetch operation. */
    private String loadFromNetwork(String urlString) throws IOException {
        InputStream stream = null;
        String str ="";

        try {
            stream = downloadUrl(urlString);
            str = readIt(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        return str;
    }

    /**
     * Given a string representation of a URL, sets up a connection and gets
     * an input stream.
     * @param urlString A string representation of a URL.
     * @return An InputStream retrieved from a successful HttpURLConnection.
     * @throws java.io.IOException
     */
    private InputStream downloadUrl(String urlString) throws IOException {

        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Start the query
        conn.connect();
        InputStream stream = conn.getInputStream();
        return stream;
    }

    /**
     * Reads an InputStream and converts it to a String.
     * @param stream InputStream containing HTML from www.google.com.
     * @return String version of InputStream.
     * @throws java.io.IOException
     */
    private String readIt(InputStream stream) throws IOException {

        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        for(String line = reader.readLine(); line != null; line = reader.readLine())
            builder.append(line);
        reader.close();
        return builder.toString();
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void setLocation()
    {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location typo provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);

        // Initialize the location fields
        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(location);
        } else {
            if(locationManager.getProvider("network") != null) {
                String localProvider = locationManager.getProvider("network").getName();
                Location location1 = locationManager.getLastKnownLocation(localProvider);
                latitude = location1.getLatitude();
                longitude = location1.getLongitude();
            }
            else if(locationManager.getProvider("passive") != null){
                String localProvider = locationManager.getProvider("passive").getName();
                Location location1 = locationManager.getLastKnownLocation(localProvider);
                latitude = location1.getLatitude();
                longitude = location1.getLongitude();
            }
            else
            {
                latitude = 0.0;
                longitude = 0.0;
            }
        }
    }
}
