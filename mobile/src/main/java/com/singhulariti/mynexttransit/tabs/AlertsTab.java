package com.singhulariti.mynexttransit.tabs;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.singhulariti.mynexttransit.Constants;
import com.singhulariti.mynexttransit.R;
import com.singhulariti.mynexttransit.Services;
import com.singhulariti.mynexttransit.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import static com.singhulariti.mynexttransit.Constants.*;


public class AlertsTab extends Fragment implements AsyncTaskCompleteListener, SwipeRefreshLayout.OnRefreshListener{
    SwipeRefreshLayout swipeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.alerts_tab,container,false);
        swipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        // Set a toolbar to replace the action bar.
        updateListIfNetwork();
        return v;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager  = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        boolean isAvailable = false;
        if(networkInfo != null && networkInfo.isConnected()){
            isAvailable = true;
        }else{
            Toast.makeText(getActivity(), "Network is unavailable.Please get connected.", Toast.LENGTH_LONG).show();
        }

        return isAvailable;
    }

    @Override
    public void onTaskComplete(JSONArray result) throws JSONException {
        Utils.updateAlertsList(getActivity(), result);
        swipeLayout.setRefreshing(false);
    }

    public void onResume() {
        super.onResume();
        updateListIfNetwork();
    }

    private void updateListIfNetwork()
    {
        if(isNetworkAvailable()) {
            new Services.GetRemoteData(this, getActivity()).execute(NJ_TRANSIT_BUS_SERVICE + ALERTS, Constants.Timed.UN_TIMED.toString());
        }
    }

    @Override
    public void onRefresh() {
        updateListIfNetwork();
    }
}
