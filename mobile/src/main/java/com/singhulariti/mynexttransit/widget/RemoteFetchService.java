package com.singhulariti.mynexttransit.widget;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.singhulariti.mynexttransit.Constants;

public class RemoteFetchService extends Service implements LocationListener {

    private int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    private AQuery aquery;
    private String remoteJsonUrl = Constants.NJ_TRANSIT_BUS_SERVICE;
    private LocationManager locationManager;
    private String provider;
    double latitude, longitude;

    public static ArrayList<ListItem> listItemList;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    /*
     * Retrieve appwidget id from intent it is needed to update widget later
     * initialize our AQuery class
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        setLocation();
        if (intent.hasExtra(AppWidgetManager.EXTRA_APPWIDGET_ID))
            appWidgetId = intent.getIntExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        aquery = new AQuery(getBaseContext());
        fetchDataFromWeb();
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * method which fetches data(json) from web aquery takes params
     * remoteJsonUrl = from where data to be fetched String.class = return
     * format of data once fetched i.e. in which format the fetched data be
     * returned AjaxCallback = class to notify with data once it is fetched
     */
    private void fetchDataFromWeb() {
        String url = remoteJsonUrl + "nearest?lat=" + latitude + "&lon=" + longitude;
        aquery.ajax(url, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String result, AjaxStatus status) {
                processResult(result);
                super.callback(url, result, status);
            }
        });
    }

    /**
     * Json parsing of result and populating ArrayList<com.singhulariti.mynexttransit.widget.ListItem> as per json
     * data retrieved from the string
     */
    private void processResult(String result) {
        Log.i("Resutl",result);
        listItemList = new ArrayList<ListItem>();
        try {
            JSONObject jsonArray = new JSONObject(result);
            JSONArray routes = jsonArray.getJSONArray("current_routes");
            if (routes != JSONObject.NULL && routes.length() > 0) {
                for (int i = 0; i < routes.length(); i++) {
                    JSONObject jObj = (JSONObject) routes.get(i);
                    String time = "";
                    try{
                        time = jObj.getString("time_number");
                    }
                    catch (NumberFormatException ne)
                    {
                        time = "";
                    }

                    ListItem listItem = new ListItem();
                    listItem.heading = jObj.getString("route_id") + " towards "
                            + jObj.getString("current_location");
                    listItem.content = time + " " + jObj.getString("time_unit");
                    listItemList.add(listItem);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(e.getMessage(), "Widget error!");
            ListItem listItem = new ListItem();
            listItem.heading = "No Services found!";
            listItem.content = "";
            listItemList.add(listItem);
        }
        populateWidget();
    }

    /**
     * Method which sends broadcast to com.singhulariti.mynexttransit.widget.WidgetProvider
     * so that widget is notified to do necessary action
     * and here action == com.singhulariti.mynexttransit.widget.WidgetProvider.DATA_FETCHED
     */
    private void populateWidget() {

        Intent widgetUpdateIntent = new Intent();
        widgetUpdateIntent.setAction(WidgetProvider.DATA_FETCHED);
        widgetUpdateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                appWidgetId);
        sendBroadcast(widgetUpdateIntent);

        this.stopSelf();
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
        setLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    private void setLocation()
    {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);

        // Initialize the location fields
        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(location);
        } else {
            if(locationManager.getProvider("network") != null) {
                String localProvider = locationManager.getProvider("network").getName();
                Location location1 = locationManager.getLastKnownLocation(localProvider);
                latitude = location1.getLatitude();
                longitude = location1.getLongitude();
            }
            else if(locationManager.getProvider("passive") != null){
                String localProvider = locationManager.getProvider("passive").getName();
                Location location1 = locationManager.getLastKnownLocation(localProvider);
                latitude = location1.getLatitude();
                longitude = location1.getLongitude();
            }
            else
            {
                latitude = 0.0;
                longitude = 0.0;
            }
        }
    }
}
