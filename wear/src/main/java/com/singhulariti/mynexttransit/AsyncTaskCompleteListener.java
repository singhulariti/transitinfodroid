package com.singhulariti.mynexttransit;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Vivek on 4/5/14.
 */
public interface AsyncTaskCompleteListener {
    public void onTaskComplete(JSONArray result) throws JSONException;
}
