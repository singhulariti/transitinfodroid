package com.singhulariti.mynexttransit;

/**
 * Created by Saurabh on 4/5/14.
 */

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Services {

    public static final String NJ_TRANSIT_BUS_SERVICE = "http://54.149.89.156:7020/";

    public static class GetDetailsByLocation extends AsyncTask<String,Void,JSONArray> {

        private AsyncTaskCompleteListener callback;

        public GetDetailsByLocation(AsyncTaskCompleteListener act) {
            this.callback = act;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONArray doInBackground(String... params) {
            JSONObject result1 = Utils.getJSONObject(NJ_TRANSIT_BUS_SERVICE + "nearest?lat="
                    + params[0] + "&lon=" + params[1]);
            try {
                if (result1 == null || result1.getBoolean("error")) return null;
            } catch (JSONException e) {
                return null;
            }
            JSONArray res = new JSONArray();
            res.put(result1);
            return res;
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            super.onPostExecute(result);
            try {
                callback.onTaskComplete(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
