package com.singhulariti.mynexttransit;

/**
 * Created by Vivek on 4/5/14.
 */

import android.app.Activity;
import android.view.View;
import android.widget.ListView;

import com.singhulariti.mynexttransit.adapters.BaseInflaterAdapter;
import com.singhulariti.mynexttransit.adapters.CardItemData;
import com.singhulariti.mynexttransit.adapters.inflaters.CardInflater;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Utils {

    public static JSONObject getJSONObject(String url) {
        JSONObject jsonResponse = null;
        HttpURLConnection linkConnection = null;
        try {
            URL linkurl = new URL(url);
            linkConnection = (HttpURLConnection) linkurl.openConnection();
            int responseCode = linkConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = linkConnection.getInputStream();
                Reader reader = new InputStreamReader(inputStream, "UTF-8");
                int contentLength = linkConnection.getContentLength();
                char[] charArray = new char[contentLength];
                int hasRead = 0;
                while (hasRead < contentLength)
                    hasRead += reader.read(charArray, hasRead, contentLength-hasRead);

                inputStream.close();
                String responseData = new String(charArray);
                jsonResponse = new JSONObject(responseData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (linkConnection != null) {
                linkConnection.disconnect();
            }
        }
        return jsonResponse;
    }

    public static void updateList(Activity activity, JSONArray result) throws JSONException {
        if (activity != null){
            BaseInflaterAdapter<CardItemData> adapter = new BaseInflaterAdapter<>(new CardInflater());
            ListView listView = (ListView) activity.findViewById(R.id.list);
            listView.addHeaderView(new View(activity));
            listView.addFooterView(new View(activity));
            if (result != null) {
                JSONArray routes = result.getJSONObject(0).getJSONArray("current_routes");
                if (routes != JSONObject.NULL && routes.length() > 0) {
                    for (int i = 0; i < routes.length(); i++) {
                        JSONObject jObj = (JSONObject) routes.get(i);
                        String time = "";
                        try {
                            time = jObj.getString("time_number");
                        } catch (NumberFormatException ne) {
                            time = "";
                        }

                        CardItemData data = new CardItemData(jObj.getString("route_id"), time,
                                jObj.getString("time_unit"), jObj.getString("current_location"));
                        adapter.addItem(data, false);
                    }
                }
            } else {
                CardItemData data = new CardItemData("No Services", "noservice", "", "");
                adapter.addItem(data, false);
            }
            listView.setAdapter(adapter);
        }
    }


    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
}
